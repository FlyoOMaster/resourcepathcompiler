package 
{
	
	/**
	 * ...
	 * @author Yarcho
	 */
	public interface IResorcePathCompiler 
	{
		function get rootFolder():String;
		function get resourceFileName():String;
		function set resourceFileName(value:String):void;
		
		function get resourceJSON():String;
		
		function scanFolder(path:String):String;
		function writeTo(pathToDirectory:String):void;
	}
	
}