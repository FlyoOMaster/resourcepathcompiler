package
{
	import flash.display.Shape;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.Dictionary;
	import flash.filesystem.*;
	
	/**
	 * ...
	 * @author Yarcho
	 */
	public class Main extends Sprite 
	{
		private var _resourceCompiler:ResorcePathCompiler; 
		private var _tfDirectories:TextField;
		private var _btnSearch:Sprite;
		
		public function Main() 
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			_tfDirectories = new TextField();
			var format:TextFormat = new TextFormat(null, 16);
			
			
			_tfDirectories.defaultTextFormat = format;
			_tfDirectories.width = stage.stageWidth;
			_tfDirectories.height = stage.stageHeight;			
			this.addChild(_tfDirectories);
			
			var textField:TextField = new TextField();
			textField.defaultTextFormat = new TextFormat(null, 16);
			textField.name = "textField";
			textField.mouseEnabled = false;
			

			var rectangleShape:Shape = new Shape();
			rectangleShape.graphics.beginFill(0xEFF0F1);
			rectangleShape.graphics.drawRect(0, 0, 100, 25);
			rectangleShape.graphics.endFill();

			_btnSearch = new Sprite();
			_btnSearch.addChild(rectangleShape);
			_btnSearch.addChild(textField);

			var tf:TextField = TextField(_btnSearch.getChildByName("textField"));
			tf.text = "Scan and Write";
			tf.x = (rectangleShape.width - tf.width) >> 1;
			_btnSearch.x = (stage.stageWidth - _btnSearch.width) >> 1;
			
			this.addChild(_btnSearch);
			
			_btnSearch.addEventListener(MouseEvent.CLICK, onClick);
			
		}
		
		private function onClick(e:MouseEvent):void 
		{
			var f : File = new File;
			f.addEventListener(Event.SELECT, onFolderSelected);
			f.browseForDirectory("Choose a directory");		
		}
		
		private function onFolderSelected(e:Event):void 
		{
			var folder:File = e.target as File;
			_resourceCompiler = new ResorcePathCompiler('resourceList.json');
						
			_resourceCompiler.scanFolder(folder.nativePath);
			_tfDirectories.text = "Scan Complete\n" + _resourceCompiler.resourceJSON;
			
			_resourceCompiler.writeTo(_resourceCompiler.rootFolder);
			_tfDirectories.text = "Write Complete\n" + _resourceCompiler.resourceJSON;	
		}		
		
	}
	
}