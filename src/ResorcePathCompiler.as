package 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author Yarcho
	 */
	public class ResorcePathCompiler implements IResorcePathCompiler
	{
		private var _resourceFileName:String;
		private var _files:Dictionary;
		private var _root:File;
		
		private var _resourceJSON:String;
		
		public function get rootFolder():String 
		{
			return _root.nativePath;
		}
		
		public function get resourceFileName():String 
		{
			return _resourceFileName;
		}
		
		public function set resourceFileName(value:String):void 
		{
			_resourceFileName = value;
		}
				
		public function get resourceJSON():String 
		{
			return _resourceJSON;
		}
		
		
		public function ResorcePathCompiler(fileName:String) 
		{
			_resourceFileName = fileName;
			_files = new Dictionary();
		}
		
		public function scanFolder(path:String):String 
		{
			_root = new File(path);		
			
			var file:File = new File(path +'/' + resourceFileName);
			_files = new Dictionary();
			
			//_stream = new FileStream();
			//_stream.open(file, FileMode.WRITE);
			var res:String = scanDirectory(_root, "");
			
			_resourceJSON = '{' + res.slice(0, res.length - 1)+ '\n}';
			//_stream.writeUTFBytes(_resourceJSON);		
			//_stream.close();
			return _resourceJSON;
			
		}
		
		private function addDirectory(path:String, directory:Array):Array 
		{
			var listOfFiles:Array = [];
			var l:int = directory.length;
			for (var i:int = 0; i < l; i++) 
			{				
				if (directory[i] is String) 
				{
					var line:String = '\n\t"' + path + '":"' + directory[i] + '",';
					listOfFiles.push(line);
				} 
			}
			return listOfFiles;
		}
		
		
		private function scanDirectory(file:File, parent:String):String 
		{
			var contents:Array = file.getDirectoryListing();
			var keyPath:String = "";
			var filePath:String = "";
			var folderPath:String = "";
			var res:String = "";
			for (var i:uint = 0; i < contents.length; i++)  
			{ 
				var content:File = contents[i];
				//Если объект директория
				if (content.isDirectory){
					folderPath = (parent == "") ? content.name : parent + '/' + content.name;
					res += scanDirectory(content, folderPath);
				} else 
				{
					//Если это корневая папка и объект это перезаписываемый файл, то пропускаем
					if (_root.name == file.name && content.name == resourceFileName)  continue;
					
					keyPath = content.name.replace("." + content.extension, "");
					if (keyPath == '') continue;
					if(!_files[keyPath]) {
						_files[keyPath] = [];
					}
					filePath =  parent + '/' + content.name;
					if(_files[keyPath] is Array) {
						res += ('\n\t"'+keyPath+'":"'+filePath+'",');
					}
				}
			}
			return res;
		}
			
		public function writeTo(pathToDirectory:String):void 
		{
			
			var stream:FileStream = new FileStream(),
				file:File = _root.resolvePath(pathToDirectory +'/' + resourceFileName);
			
			stream.open(file, FileMode.WRITE);
			stream.writeUTFBytes(resourceJSON);	
		}		
	}
}